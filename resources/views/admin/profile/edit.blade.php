@extends('adminlte::page')

@section('content_header')
    <h1>{{ trans('profile.edit_profile') }}</h1>
@stop

@section('content')
    {!! Form::model($profile, ['url' => route('admin.profile.update', $profile), 'method' => 'POST', 'files' => true ]) !!}
    <input name="_method" type="hidden" value="PUT">
    @include('admin.profile.partials._form', ['mode' => 'edit'])
    {!! Form::close() !!}
@stop
