@extends('adminlte::page')

@section('content_header')
    <h1>Под {{ trans('category.title') }}</h1>
@stop

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="page-actions">
                <a href="{{ route('admin.parent-categories.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> {{ trans('category.new') }}</a>
            </div>
        </div>
        <br>
        <div class="box-body">
{{--            <form action="{{ route('admin.categories.index') }}">--}}
{{--                <div class="form-group" style="margin-top: 20px;">--}}
{{--                    <div class="input-group">--}}
{{--                        <input type="text" name="search" class="form-control" placeholder="{{  $filter->search !=null ? $filter->search : 'Поиск...' }}" style="border-color: #ccc">--}}
{{--                        <span class="input-group-btn">--}}
{{--                            <button class="btn btn-flat" type="submit" style="border-color: #ccc"><i class="fa fa-search"></i></button>--}}
{{--                        </span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </form>--}}
            <table class="table table-hover table-striped no-margin">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>{{ trans('category.name') }}</th>
                    <th>{{ trans('category.events') }}</th>
                </tr>
                </thead>
                <tbody class="sortable" id="sortable">
                @if (isset($categories))
                    @foreach ($categories as $category)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td>{{ $category->title }}</td>
                            <td>
                                @if(Auth::user()->can('admin-panel'))
                                    <a class="btn btn-primary btn-sm"
                                       title="Редактировать"
                                       href="{{ route('admin.parent-categories.edit', $category->id) }}"
                                       data-toggle="tooltip"
                                       data-placement="top">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                @endif

                                @if(Auth::user()->can('admin-panel'))
                                    {!! Form::open([
                                           'method' => 'DELETE',
                                           'url' => [route('admin.categories.destroy', $category->id)],
                                           'style' => 'display:inline'
                                       ]) !!}
                                    {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-sm',
                                            'title' => 'Удалить жанр',
                                            'onclick'=>'return confirm("Вы действительно хотите удалить?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div style="display: flex; justify-content: center">
                {{ $categories->links() }}
            </div>
        </div>
    </div>
@stop
