@extends('adminlte::page')

@section('content_header')
    <h1>{{ $animal->name_animal }}</h1>
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="btn-group">
                <a href="{{ route('admin.animals.index') }}" class="btn btn-default">
                    <i class="fa fa-arrow-left"></i>
                    <span class="hidden-xs hidden-sm">{{ trans('animals.back') }}</span>
                </a>
{{--                <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> {{ trans('animals.save') }}</button>--}}
            </div>
        </div>
        <br>
        <div class="box-body">
            <div class="animal-content">
                <div class="image-block">
                    <img src="{{ $animal->animal_file }}" alt="image not available">
                </div>
                <div class="animal-user">
                    <p>{{ $animal->user->name }}</p>
                    <p>{{ $animal->user->email }}</p>
                </div>
            </div>
        </div>
    </div>
@stop
