<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AnimalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name_animal' => 'required|unique:animals,name_animal',
            'user_id' => 'required',
        ];

        if ($this->method() !== 'POST') {
            $rules = [
                'name_animal' => 'required|unique:animals,name_animal,' . $this->animal->id,
            ];
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'name_animal.unique' => trans('request.unique'),
            'name_animal.required' => trans('request.required'),
            'user_id.required' => trans('request.required'),
        ];
    }
}
