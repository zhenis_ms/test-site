@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<div class="box box-primary">
    <div class="box-header with-border">
        <div class="form-actions">
            <div class="btn-group">
                <a href="{{ route('admin.animals.index') }}" class="btn btn-default">
                    <i class="fa fa-arrow-left"></i>
                    <span class="hidden-xs hidden-sm">{{ trans('animals.back') }}</span>
                </a>
                <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> {{ trans('animals.save') }}</button>
            </div>
        </div>
    </div>
    @if($errors->any())
        <div class="alert alert-danger">{{ trans('animals.error') }}</div>
    @endif
    <div class="box-body">
        <div class="tab-pane">
            <div class="form-group {{ $errors->has('name_animal') ? 'has-error' :'' }}">
                {{ Form::label('name_animal', trans('animals.name_animal')) }}
                {{ Form::text('name_animal', $animal->name_animal, ['class' => 'form-control']) }}
                {!! $errors->first('name_animal','<span class="help-block">:message</span>') !!}
                <br>
                {{ Form::label('animal_file', 'Видео') }}
                {{ Form::file('animal_file', ['class' => 'form-control']) }}
                {!! $errors->first('animal_file','<span class="help-block">:message</span>') !!}
            </div>

            <div class="form-group {{ $errors->has('user_id') ? 'has-error' :'' }}">
                <label for="user_id">{{ trans('animals.selecting_user') }}</label>
                <select id="user_id" name="user_id" class="form-control">
                    <option selected value=''>{{ trans('animals.select_user') }}</option>
                    @if(isset($users))
                        @foreach($users as $user)
                            <option value="{{ $user->id }}" {{ $user->id == $animal->user_id ? 'selected' : '' }}>
                                {{ $user->name }}
                            </option>
                        @endforeach
                    @endif
                </select>
                {!! $errors->first('user_id','<span class="help-block">:message</span>') !!}
            </div>
        </div>
    </div>
</div>
