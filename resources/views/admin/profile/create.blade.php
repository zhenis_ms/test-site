@extends('adminlte::page')

@section('content_header')
    <h1>{{ trans('profile.create_animal') }}</h1>
@stop

@section('content')
    {!! Form::open(['url' => route('admin.profile.store'), 'files' => true]) !!}
    @include('admin.profile.partials._form', ['mode' => 'create'])
    {!! Form::close() !!}
@stop
