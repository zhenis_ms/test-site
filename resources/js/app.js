require('./bootstrap');
// require('./datepicker-ru');

window.Vue = require('vue');

import VueRouter from 'vue-router';
import  router from './router';
import App from "./components/App";

const app = new Vue({
    el: '#app',
    render : h => h(App),
    router
});
