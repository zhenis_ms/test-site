@extends('adminlte::page')

@section('content_header')
    <h1>{{ trans('user.title') }}</h1>
@stop

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    <div class="box box-primary">
        <div class="page-actions">
            <a href="{{ route('admin.profile.edit') }}" class="btn btn-primary"> {{ trans('profile.new_user') }}</a>
        </div>
        <br>
        <div class="box-body">
            {{ $user->name }}
            @if ($profile)
                <div>{{ $profile->year }}</div>
                <div>{{ $profile->city }}</div>
                @if ($profile->image)
                    <div class="image-block">
                        <img src="{{ $profile->image }}" alt="image not available">
                    </div>
                @endif
            @endif
        </div>
    </div>
@stop
