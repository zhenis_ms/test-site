@extends('adminlte::page')

@section('content_header')
    <h1>{{ trans('animals.title') }}</h1>
@stop

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-info">{{ Session::get('message') }}</div>
    @endif
    @if (Session::has('error'))
        <div class="alert alert-danger">{{ Session::get('error') }}</div>
    @endif
    <div class="box box-primary">
        <div class="box-header with-border">
            <div class="page-actions">
                <a href="{{ route('admin.animals.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> {{ trans('animals.new_user') }}</a>
            </div>
        </div>
        <br>
        <div class="box-body">

            <form action="{{ route('admin.animals.index') }}">
                <div>
                    <p style="margin-left: 15px">{{ trans('animals.count') }} {{ $animals->count() }}</p>
                </div>
                <div class="form-group-wraper">
                    <div class="form-group col-sm-3">
                        <select id="user_id" name="user_id" class="form-control">
                            <option selected value=''>{{ trans('manuscripts.select_user') }}</option>
                            @foreach ($users as $user)
                                <option value="{{ $user->id }}" {{ $user->id == $filter->user_id ? 'selected' : '' }}>{{ $user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-2">
                        <button type="submit" class="btn-primary btn btn-filter"><i class="fa fa-filter"></i>{{ trans('manuscripts.filter') }}</button>
                    </div>
                </div>
            </form>
            <table class="table table-hover table-striped no-margin">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>{{ trans('animals.name_animal') }}</th>
                    <th>{{ trans('animals.user') }}</th>
                    <th>{{ trans('animals.events') }}</th>
                </tr>
                </thead>
                <tbody class="sortable" id="sortable">
                @if (isset($animals))
                    @foreach ($animals as $animal)
                        <tr>
                            <td>{{ $animal->id ?? "" }}</td>
                            <td>{{ $animal->name_animal ?? "" }}</td>
                            <td>{{ $animal->user->id ?? ""}}</td>
                            <td>

                                @if(Auth::user()->can('admin-panel'))
                                    <a class="btn btn-primary btn-sm"
                                       title="Редактировать"
                                       href="{{ route('admin.animals.edit', $animal->id) }}"
                                       data-toggle="tooltip"
                                       data-placement="top">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                @endif
                                    <a class="btn btn-primary btn-sm"
                                       title="Просмотр"
                                       href="{{ route('admin.animals.show', $animal->id) }}"
                                       data-toggle="tooltip"
                                       data-placement="top">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a class="btn btn-primary btn-sm"
                                       title="Просмотр"
                                       href="{{ route('admin.animals.download', $animal->id) }}"
                                       data-toggle="tooltip"
                                       data-placement="top">
                                        <i class="fa fa-download"></i>
                                    </a>
{{--                                    @can('download')--}}
{{--                                        <a href="animals/download/{{ $animal->id }}" class="" data-position="left" data-delay="50" data-tooltip="{{ trans('docs.download') }}"><i class="material-icons">file_download</i></a>--}}
{{--                                    @endcan--}}
                                @if(Auth::user()->can('admin-panel'))
                                    {!! Form::open([
                                           'method' => 'DELETE',
                                           'url' => [route('admin.animals.destroy', $animal->id)],
                                           'style' => 'display:inline'
                                       ]) !!}

                                    {!! Form::button('<i class="fa fa-trash" aria-hidden="true"></i>', array(
                                            'type' => 'submit',
                                            'class' => 'btn btn-danger btn-sm',
                                            'title' => 'Удалить животное',
                                            'onclick'=>'return confirm("Вы действительно хотите удалить?")'
                                    )) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <div style="display: flex; justify-content: center">
                {{ $animals->links() }}
            </div>
        </div>
    </div>
@stop
