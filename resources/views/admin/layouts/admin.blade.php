@extends('admin.layouts.app_admin')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/skins/skin-' . config('adminlte.skin', 'blue') . '.min.css')}} ">
    <link rel="stylesheet" href="{{asset('/admin/plugins/datetimepicker/bootstrap-datetimepicker.min.css')}}">
    @stack('css')
    @yield('css')
@stop

@section('body_class', 'skin-' . config('adminlte.skin', 'blue') . ' sidebar-mini ' . (config('adminlte.layout') ?
    [
        'boxed' => 'layout-boxed',
        'fixed' => 'fixed',
        'top-nav' => 'layout-top-nav'
    ][config('adminlte.layout')] : '') . (config('adminlte.collapse_sidebar') ? ' sidebar-collapse ' : ''))

@section('body')
    <div class="wrapper">
        <header class="main-header">
            @if(config('adminlte.layout') == 'top-nav')
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="{{ url(config('adminlte.dashboard_url', '/')) }}" class="navbar-brand">
                                {!! config('adminlte.logo', '<b>Admin</b>LTE') !!}
                            </a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
                            <ul class="nav navbar-nav">
                            </ul>
                        </div>
                        @else
                            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}" class="logo">
                                <span class="logo-mini">{!! config('adminlte.logo_mini', '<b>Admin</b>LTE') !!}</span>
                                <span class="logo-lg">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</span>
                            </a>
                            <nav class="navbar navbar-static-top" role="navigation">
                                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                                    <span class="sr-only">{{ trans('adminlte::adminlte.toggle_navigation') }}</span>
                                </a>
                                @endif
                                <div class="navbar-custom-menu">
                                    <ul class="nav navbar-nav">
                                        <li>
                                            @if(config('adminlte.logout_method') == 'GET' || !config('adminlte.logout_method') && version_compare(\Illuminate\Foundation\Application::VERSION, '5.3.0', '<'))
                                                <a href="{{ url('inside/logout') }}">
                                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                                </a>
                                            @else
                                                <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                    <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                                </a>
                                                <form id="logout-form" action="{{ url('inside/logout') }}" method="POST" style="display: none;">
                                                    @if(config('adminlte.logout_method'))
                                                        {{ method_field(config('adminlte.logout_method')) }}
                                                    @endif
                                                    {{ csrf_field() }}
                                                </form>
                                            @endif
                                        </li>
                                    </ul>
                                </div>
                            @if(config('adminlte.layout') == 'top-nav')
                    </div>
                    @endif
                </nav>
        </header>
        @if(config('adminlte.layout') != 'top-nav')
            <aside class="main-sidebar">
                <section class="sidebar">
                    <ul class="sidebar-menu" data-widget="tree">
                        <li class="header">ПАНЕЛЬ СОСТОЯНИЯ</li>
                        <li class="">
                            <a href="{{url('/inside')}}">
                                <i class="fa fa-dashboard"></i>
                                <span>dashboard</span>
                            </a>
                        </li>
                        <li class="header">Контент</li>
                        <li class="">
                            <a href="#">
                                <i class="fa fa-fw fa fa-book"></i>
                                <span>Разделы</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#"><i class="fa fa fa-graduation-cap"></i> <span>Раздел Учителю</span>
                                <span class="pull-right-container">
                                   <i class="fa fa-angle-left pull-right"></i>
                                 </span>
                            </a>
                            <ul class="treeview-menu">
                                <li class="">
                                    <a href="#">
                                        <i class="fa fa-tags"></i>
                                        <span>Поурочные планы</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#">
                                        <i class="fa fa-youtube-play"></i>
                                        <span>Вебинары</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#">
                                        <i class="fa fa-book"></i>
                                        <span>Видео разделы</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#">
                                        <i class="fa fa-link"></i>
                                        <span>Видео материалы</span>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="#">
                                        <i class="fa fa-link"></i>
                                        <span>Уроки и Классные часы</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="">
                            <a href="#">
                                <i class="fa fa-fw fa-newspaper-o"></i>
                                <span>Новости</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#">
                                <i class="fa fa-fw fa-link"></i>
                                <span>Статьи</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#">
                                <i class="fa fa-language"></i>
                                <span>Переводы</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#">
                                <i class="fa fa-fw fa-link"></i>
                                <span>Уроки</span>
                            </a>
                        </li>
                        <li class="">
                            <a href="#">
                                <i class="fa fa-fw fa-calendar"></i>
                                <span>Месяцы</span>
                            </a>
                        </li>
                    </ul>
                </section>
            </aside>
        @endif
        <div class="content-wrapper">
            @if(config('adminlte.layout') == 'top-nav')
                <div class="container">
                    @endif
                    <section class="content">
                        @yield('content')
                        {{--<!-- .modal-dialog -->--}}
                        {{--<div class="modal modal-danger fade" role="dialog" id="modal-destroy">--}}
                        {{--<div class="modal-dialog">--}}
                        {{--<div class="modal-content">--}}
                        {{--<div class="modal-header">--}}
                        {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                        {{--<span aria-hidden="true">&times;</span>--}}
                        {{--</button>--}}
                        {{--<h4 class="modal-title">Внимание</h4>--}}
                        {{--</div>--}}
                        {{--<div class="modal-body">--}}
                        {{--<p>Вы уверены что хотите удалить?</p>--}}
                        {{--</div>--}}
                        {{--<div class="modal-footer">--}}
                        {{--<button type="button" class="btn btn-outline pull-left" data-action="yes_destroy">Да</button>--}}
                        {{--<button type="button" class="btn btn-outline" data-dismiss="modal">Нет</button>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--<!-- /.modal-dialog -->--}}
                    </section>
                    @if(config('adminlte.layout') == 'top-nav')
                </div>
            @endif
        </div>
    </div>
@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script src="/admin/plugins/jQueryUI/jquery-ui.js"></script>
    <script src="/admin/plugins/growl/jquery.growl.js"></script>
    <script src="/admin/plugins/jQueryForm/jquery.form.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script src="/admin/plugins/moment/moment-with-locales.min.js"></script>
    <script src="/admin/plugins/datetimepicker/bootstrap-datetimepicker.min.js"></script>
    <script>
        $('.ckeditor').ckeditor();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    <script>
        $(document).ready(function(){
            /** add active class and stay opened when selected */
            var url = window.location;
            // for sidebar menu entirely but not cover treeview
            $('ul.sidebar-menu a').filter(function() {
                return this.href == url;
            }).parent().addClass('active');

            // for treeview
            $('ul.treeview-menu a').filter(function() {
                return this.href == url;
            }).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
        });
    </script>
    @stack('js')
    @yield('js')
@stop
