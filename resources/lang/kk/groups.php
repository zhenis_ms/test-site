<?php

return [
    'success_save' => 'Деректер сәтті сақталды!',
    'success_destroy' => 'Деректер сәтті жойылды',
    'success_password_reset' => 'Пароль сәтті жаңартылды',
];
