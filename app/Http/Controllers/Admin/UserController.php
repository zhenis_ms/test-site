<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $users = User::query();

        if ($value = $request->input('search')) {

            $users = $users->where('name', 'like', '%' . $value . '%');
        }

        $users = $users->paginate(20);

        return view('admin.users.index', [
            'users' => $users,
            'filter' => $request
        ]);
    }
}
