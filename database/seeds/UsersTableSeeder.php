<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $root = new User();
        $root->name = 'Admin';
        $root->email = 'admin@gmail.com';
        $root->password = bcrypt('12345678');
        $root->role = 10;
        $root->save();
    }
}
