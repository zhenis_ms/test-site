<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\ParentCategory;

class ParentCategoryController extends Controller
{
    public function index()
    {
        $categories = ParentCategory::query()->paginate(20);
        return view('admin.parent-categories.index', [
            'categories' => $categories,
        ]);
    }

    public function create()
    {
        return view('admin.parent-categories.create', [
            'category' =>  [],
            'categories' => ParentCategory::with('children')->where('parent_id', 0)->get(),
            'delimiter' => ''
        ]);
    }

    public function store(Request $request)
    {
        ParentCategory::create($request->all());

        return redirect()->route('admin.parent-categories.index');
    }

    public function show(ParentCategory $category)
    {

    }

    public function edit(ParentCategory $category)
    {
        return view('admin.parent-categories.edit', [
            'category' =>  $category,
            'categories' => ParentCategory::with('children')->where('parent_id', 0)->get(),
            'delimiter' => ''
        ]);
    }

    public function update(Request $request, ParentCategory $category)
    {
        $category->update($request->all());

        return redirect()->route('admin.parent-categories.index');
    }
}
