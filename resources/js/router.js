import VueRouter from "vue-router";
import  Users from './components/Users';


export  default  new VueRouter({
    routers: [
        {
            path: '/main/index',
            component: Users
        }
    ],
    mode: 'history'
});
