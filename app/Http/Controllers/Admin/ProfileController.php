<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        $user = Auth::user();
        $profile = Profile::where('user_id', Auth::user()->id)->first();

        return view('admin.profile.index', [
            'user' => $user,
            'profile' => $profile,
        ]);
    }

    public function edit()
    {
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        return view('admin.profile.edit', [
            'profile' => $profile,
        ]);
    }

    public function  update(Request $request)
    {
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        $profile->update([
            'year' => $request->get('year'),
            'city' => $request->get('city'),
        ]);

        if ($request->hasFile('image')) {
            Storage::disk('public')->deleteDirectory('profiles/'.$profile->id);
            $profile->image = $request->file('image')->store('profiles/'.$profile->id, 'public');
            $profile->update();
        }

        Session::flash('message', trans('groups.success_save'));
        return redirect()->to(route('admin.profile.index'));
    }


}
