<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'user_id', 'image', 'year', 'city'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getImageAttribute($value)
    {
        return '/storage/' . $value;
    }
}
