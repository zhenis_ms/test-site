{{--@if (Session::has('message'))--}}
{{--    <div class="alert alert-info">{{ Session::get('message') }}</div>--}}
{{--@endif--}}
{{--<div class="box box-primary">--}}
{{--    <div class="box-header with-border">--}}
{{--        <div class="form-actions">--}}
{{--            <div class="btn-group">--}}
{{--                <a href="{{ route('admin.parent-categories.index') }}" class="btn btn-default">--}}
{{--                    <i class="fa fa-arrow-left"></i>--}}
{{--                    <span class="hidden-xs hidden-sm">{{ trans('category.back') }}</span>--}}
{{--                </a>--}}
{{--                <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> {{ trans('category.save') }}</button>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    @if($errors->any())--}}
{{--        <div class="alert alert-danger">{{ trans('category.error') }}</div>--}}
{{--    @endif--}}
{{--    <div class="box-body">--}}
{{--        <div class="tab-pane">--}}
{{--            <div class="form-group {{ $errors->has('name') ? 'has-error' :'' }}">--}}
{{--                {{ Form::label('name', trans('category.name')) }}--}}
{{--                {{ Form::text('name', $category->title, ['class' => 'form-control']) }}--}}
{{--                {!! $errors->first('name','<span class="help-block">:message</span>') !!}--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="form-group">
    <input type="text" class="form-control" name="title" value="{{ $category->title ?? ''}}" placeholder="Наименование категории">
</div>

<div class="form-group">
    <select name="parent_id" class="form-control">
        <option value="0">-- без родительской категории --</option>
        @include('admin.parent-categories.partials._categories')
    </select>
</div>

<hr>

<button type="submit" class="btn btn-primary">Сохранить</button>
