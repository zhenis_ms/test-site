<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use RichanFongdasen\EloquentBlameable\BlameableTrait;
use Spatie\Activitylog\Traits\LogsActivity;

class Category extends Model
{
    // use BlameableTrait, LogsActivity, SoftDeletes;

    protected $table = 'categories';

    // protected static $logOnlyDirty = true;
    //
    // protected static $logAttributes = [ 'name', 'updated_by' ];
    // protected static $logName = 'categories';

    protected $fillable = [
        'name',
        'created_by',
        'updated_by',
        'deleted_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
