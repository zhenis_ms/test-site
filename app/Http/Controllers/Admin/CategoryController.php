<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Facades\Session;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::query();

        if ($value = $request->input('search')) {

            $categories = $categories->where('name', 'like', '%' . $value . '%');
        }

        $categories = $categories->paginate(20);

        return view('admin.categories.index', [
            'categories' => $categories,
            'filter' => $request
        ]);
    }

    public function create()
    {
      return view('admin.categories.create', [
          'category' => new Category
      ]);
    }

    public function store(CategoryRequest $request)
    {
      try {
          $category = Category::create([
              'name' => $request->input('name'),
          ]);

          Session::flash('message', trans('groups.success_save'));

          return redirect()->route('admin.categories.index');
      } catch (Exception $exception) {
      }
    }

    public function show(Category $category)
    {
        //
    }

    public function edit(Category $category)
    {
        return view('admin.categories.edit', ['category' => $category]);
    }

    public function update(CategoryRequest $request, Category $category)
    {
      try {
          $category->update([
              'name' => $request->input('name'),
          ]);

          Session::flash('message', trans('groups.success_save'));

          return redirect()->to(route('admin.categories.index'));

      } catch (Exception $exception) {

      }
    }

    public function destroy(Category $category)
    {
      if ($category) {
          $category->delete();

          Session::flash('message', trans('groups.success_destroy'));

          return redirect()->to(route('admin.categories.index'));
      }

      return abort(404);
    }
}
