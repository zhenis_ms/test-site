<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AnimalRequest;
use App\Models\Animal;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class AnimalController extends Controller
{
    public function index(Request $request)
    {
        $query = Animal::query();
        $users = User::all();

        if ($request->input('user_id')) {
            $query->where('user_id', $request->user_id);
        }

        $animals = $query->paginate(10);

        return view('admin.animals.index', ['animals' => $animals, 'users' => $users, 'filter' => $request] );
    }

    public function create()
    {
        $users = User::all();

        return view('admin.animals.create', [
            'users' => $users,
            'animal' => new Animal()
        ]);
    }

    public function store(AnimalRequest $request)
    {
        $animal = Animal::create([
            'name_animal' => $request->get('name_animal'),
            'user_id' => $request->get('user_id'),
        ]);


        if ($request->hasFile('animal_file')) {
            $animal->animal_file = $request->file('animal_file')->store('animals/'.$animal->id, 'public');
            $animal->update();
        }

        Session::flash('message', trans('groups.success_save'));

        return redirect()->to(route('admin.animals.index'));
    }

    public function show(Animal $animal)
    {
        return view('admin.animals.show', [
            'animal' => $animal
        ]);
    }

    public function edit(Animal $animal)
    {
        $users = User::all();

        return view('admin.animals.edit', [
            'animal' => $animal,
            'users' => $users
        ]);
    }

    public function update(AnimalRequest $request, Animal $animal)
    {
        $animal->update([
            'name_animal' => $request->get('name_animal'),
            'user_id' => $request->get('user_id'),
        ]);

        if ($request->hasFile('animal_file')) {
            Storage::disk('public')->deleteDirectory('animals/'.$animal->id);
            $animal->animal_file = $request->file('animal_file')->store('animals/'.$animal->id, 'public');
            $animal->update();
        }

        Session::flash('message', trans('groups.success_save'));
        return redirect()->to(route('admin.animals.index'));
    }

    public function destroy(Animal $animal)
    {
        if ($animal->delete()) {
            Session::flash('message', trans('groups.success_destroy'));
        }

        return redirect()->to(route('admin.animals.index'));
    }

    public function UserAnimals(Request $request)
    {
        $user_id = $request->get('user_id');

        $animals = Animal::where('user_id', $user_id)->get();

        return response()->json([
            'animals' => $animals
        ], 200);
    }

    // download file
    public function download($id)
    {
        $animal = Animal::findOrFail($id);
        $path = Storage::disk('public')->getDriver()->getAdapter()->applyPathPrefix($animal->getRawOriginal('animal_file'));

        return response()->download($path);
    }
}
