@extends('adminlte::page')

@section('content_header')
    <h1>{{ trans('animals.create_animal') }}</h1>
@stop

@section('content')
    {!! Form::open(['url' => route('admin.animals.store'), 'files' => true]) !!}
    @include('admin.animals.partials._form', ['mode' => 'create'])
    {!! Form::close() !!}
@stop
