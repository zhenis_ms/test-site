<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParentCategory extends Model
{
    protected $table = 'parent_categories';
    protected $guarded = [];

//    protected $fillable = [
//        'title',
//        'parent_id',
//    ];

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id');
    }
}
