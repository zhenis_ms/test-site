<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Animal extends Model
{
    // protected $guarded = [];

    protected $fillable = [
        'name_animal', 'user_id', 'animal_file',
    ];

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function getAnimalFileAttribute($value)
    {
        return '/storage/' . $value;
    }
}
