@extends('adminlte::page')

@section('content_header')
    <h1>{{ trans('category.edit') }}</h1>
@stop

@section('content')
    {!! Form::model($category, ['url' => route('admin.parent-categories.update', $category), 'method' => 'POST' ]) !!}
    <input name="_method" type="hidden" value="PUT">
    @include('admin.parent-categories.partials.form', ['mode' => 'edit'])
    {!! Form::close() !!}
@stop
