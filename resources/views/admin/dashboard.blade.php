@extends('adminlte::page')

@section('content_header')
@stop

@section('content')
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3>А это заголовок</h3>
        </div>
        <div class="box-body">
            Пусть будет контентом
            <div class="dashboard">
                @foreach($animals as $animal)
                    <div class="image-block">
                        <img src="{{ $animal->animal_file }}" alt="image not available">
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop
