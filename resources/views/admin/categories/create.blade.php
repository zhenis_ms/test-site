@extends('adminlte::page')

@section('content_header')
    <h1>{{ trans('category.create') }}</h1>
@stop

@section('content')
    {!! Form::open(['url' => route('admin.categories.store')]) !!}
     @include('admin.categories.partials.form', ['mode' => 'create'])
    {!! Form::close() !!}
@stop
