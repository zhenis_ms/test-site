<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function()
{
    Route::get('/', function () {
        return view('main.index');
    });

    Route::post('/image/upload', 'ImageController@upload')->name('image.upload');

    Route::group(['prefix'=>'admin', 'as' => 'admin.', 'namespace'=>'Admin', 'middleware'=>['auth', 'can:admin-panel']], function(){
        Route::get('/', 'DashboardController@dashboard')->name('admin.index');

        Route::get('users', 'UserController@index')->name('users.index');

        //Routes for profile
        Route::get('profile', 'ProfileController@index')->name('profile.index');
        Route::get('profile/edit', 'ProfileController@edit')->name('profile.edit');
        Route::put('profile/', 'ProfileController@update')->name('profile.update');

        //Routes for categories
        Route::get('categories', 'CategoryController@index')->name('categories.index');
        Route::get('categories/create', 'CategoryController@create')->name('categories.create');
        Route::post('categories', 'CategoryController@store')->name('categories.store');
        Route::get('categories/{category}/edit', 'CategoryController@edit')->name('categories.edit');
        Route::put('categories/{category}', 'CategoryController@update')->name('categories.update');
        Route::delete('categories/{category}', 'CategoryController@destroy')->name('categories.destroy');

        //Routes for parent-categories
        Route::get('parent-categories', 'ParentCategoryController@index')->name('parent-categories.index');
        Route::get('parent-categories/create', 'ParentCategoryController@create')->name('parent-categories.create');
        Route::post('parent-categories', 'ParentCategoryController@store')->name('parent-categories.store');
        Route::get('parent-categories/{category}/edit', 'ParentCategoryController@edit')->name('parent-categories.edit');
        Route::put('parent-categories/{category}', 'ParentCategoryController@update')->name('parent-categories.update');
        Route::delete('parent-categories/{category}', 'ParentCategoryController@destroy')->name('parent-categories.destroy');

        //Routes for animals
        Route::get('animals', 'AnimalController@index')->name('animals.index');
        Route::get('animals/create', 'AnimalController@create')->name('animals.create');
        Route::post('animals', 'AnimalController@store')->name('animals.store');
        Route::get('animals/{animal}/show', 'AnimalController@show')->name('animals.show');
        Route::get('animals/{animal}/edit', 'AnimalController@edit')->name('animals.edit');
        Route::put('animals/{animal}', 'AnimalController@update')->name('animals.update');
        Route::delete('animals/{animal}', 'AnimalController@destroy')->name('animals.destroy');
        Route::get('animals/download/{id}','AnimalController@download')->name('animals.download');

        Route::get('user-animals', 'AnimalController@UserAnimals')->name('user.animals.ajax');
    });



});
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
