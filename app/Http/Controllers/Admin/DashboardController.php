<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Animal;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    //Dashboard
    public function dashboard() {

        $animals = Animal::all();

        return view('admin.dashboard', [
            'animals' => $animals,
        ]);
    }
}
