<?php

namespace App\Models;

use App\DeskList;
use Illuminate\Database\Eloquent\Model;

class Desk extends Model
{
//    use HasFactory;
    protected $fillable = ['name'];

    public function lists()
    {
        return $this->hasMany(DeskList::class);
    }
}
