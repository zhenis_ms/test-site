<?php

return [
    'success_save' => 'Данные успешно сохранены',
    'success_destroy' => 'Данные успешно удалены',
    'success_password_reset' => 'Пароль успешно обновлен',
];
