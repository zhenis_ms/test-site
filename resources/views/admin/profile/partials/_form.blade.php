@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<div class="box box-primary">
    <div class="box-header with-border">
        <div class="form-actions">
            <div class="btn-group">
                <a href="{{ route('admin.profile.index') }}" class="btn btn-default">
                    <i class="fa fa-arrow-left"></i>
                    <span class="hidden-xs hidden-sm">{{ trans('profile.back') }}</span>
                </a>
                <button type="submit" class="btn-success btn"><i class="fa fa-save"></i> {{ trans('profile.save') }}</button>
            </div>
        </div>
    </div>
    @if($errors->any())
        <div class="alert alert-danger">{{ trans('profile.error') }}</div>
    @endif
    <div class="box-body">
        <div class="tab-pane">
            <div class="form-group {{ $errors->has('year') ? 'has-error' :'' }}">
                {{ Form::label('year', trans('profile.year')) }}
                {{ Form::date('year', $profile->year, ['class' => 'form-control']) }}
                {!! $errors->first('year','<span class="help-block">:message</span>') !!}
                <br>
                {{ Form::label('city', trans('profile.city')) }}
                {{ Form::text('city', $profile->city, ['class' => 'form-control']) }}
                {!! $errors->first('city','<span class="help-block">:message</span>') !!}
                <br>
                {{ Form::label('image', 'Аватар') }}
                {{ Form::file('image', ['class' => 'form-control']) }}
                @if ($profile->image)
                    <div class="image-block">
                        <img src="{{ $profile->image }}" alt="image not available">
                    </div>
                @endif
                {!! $errors->first('image','<span class="help-block">:message</span>') !!}
            </div>
        </div>
    </div>
</div>
