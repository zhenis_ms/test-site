<?php

return [
    'title' => 'Животные',
    'name_animal' => 'название',
    'user' => 'Пользователь',
    'events' => 'Действия',
    'new_user' => 'Новое животное',
    'create_animal' => 'Создание животного',
    'edit_animal' => 'Редактирование животного',
    'back' => 'Вернуться',
    'save' => 'Сохранить',
    'selecting_box' => 'Выбрать коробку',
    'selecting_user' => 'Выбрать пользователя',
    'select_user' => 'Выбрате пользователя',
    'error' => 'Ошибка валидации',
    'cannot_delete' => 'Невозможно удалить так как есть связанные записи',
    'count' => 'количество животных:',
];
