@extends('adminlte::page')

@section('content_header')
    <h1>{{ trans('animals.edit_animal') }}</h1>
@stop

@section('content')
    {!! Form::model($animal, ['url' => route('admin.animals.update', $animal), 'method' => 'POST', 'files' => true ]) !!}
    <input name="_method" type="hidden" value="PUT">
    @include('admin.animals.partials._form', ['mode' => 'edit'])
    {!! Form::close() !!}
@stop
